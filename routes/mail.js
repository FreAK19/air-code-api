const router = require('express-promise-router')();
const { body, param } = require('express-validator/check');

const passport = require('@/passport');
const mailController = require('@/controllers/mail');
const validationMiddleware = require('@/validation/validationMiddleware');

router.route('/')
  .post(
    [
      body('email').isEmail().withMessage('Email is not valid'),
      body('message').exists()
    ],
    validationMiddleware,
    mailController.createAndSendMail
  )
  .get(
    passport.authenticateAdmin,
    validationMiddleware,
    mailController.getAllMailMessages
  );

router.route('/:mail_id')
  .delete(
    passport.authenticateAdmin,
    [param('mail_id').isMongoId()],
    validationMiddleware,
    mailController.deleteMailMessage
  );

module.exports = router;
