const router = require('express-promise-router')();
const { body } = require('express-validator/check');

const dashboardController = require('@/controllers/user');
const validationMiddleware = require('@/validation/validationMiddleware');

router.route('/')
  .post(
    [
      body('email').isEmail().withMessage('Email is not valid'),
      body('password').exists()
    ],
    validationMiddleware,
    dashboardController.login
  )

module.exports = router;
