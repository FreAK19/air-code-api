const router = require('express-promise-router')();
const { body, param } = require('express-validator/check');

const passport = require('@/passport');
const pdfController = require('@/controllers/pdf');
const validationMiddleware = require('@/validation/validationMiddleware');

router.route('/')
	.get(
		validationMiddleware,
		pdfController.getLink
	)
	.post(
		passport.authenticateAdmin,
		[
			body('link').exists(),
			body('showCvLink').exists(),
		],
		validationMiddleware,
		pdfController.createLink
	);

router.route('/:link_id')
  .put(
    passport.authenticateAdmin,
    [param('link_id').isMongoId()],
    validationMiddleware,
    pdfController.updateLink
);


module.exports = router;
