const router = require('express-promise-router')();
const { body, param } = require('express-validator/check');

const passport = require('@/passport');
const categoriesController = require('@/controllers/category');
const validationMiddleware = require('@/validation/validationMiddleware');

router.route('/')
  .get(
    validationMiddleware,
    categoriesController.getCategories
  )
  .post(
    passport.authenticateAdmin,
    [
      body('name').exists(),
    ],
    validationMiddleware,
    categoriesController.createCategory
  );

router.route('/:category_id')
  .delete(
    passport.authenticateAdmin,
    [param('category_id').isMongoId()],
    validationMiddleware,
    categoriesController.deleteCategory
  );

router.route('/:category_id')
  .put(
    passport.authenticateAdmin,
    [param('category_id').isMongoId()],
    validationMiddleware,
    categoriesController.updateCategory
  );

module.exports = router;
