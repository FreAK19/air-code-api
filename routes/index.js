const express = require('express');
const postRoutes = require('./post');
const authRoutes = require('./auth');
const linksRoutes = require('./links');
const mailingRoutes = require('./mail');
const contactRoutes = require('./contact');
const categoriesRoutes = require('./categories');

const router = express.Router();

router.use('/auth', authRoutes);
router.use('/posts', postRoutes);
router.use('/pdf', linksRoutes);
router.use('/mails', mailingRoutes);
router.use('/contacts', contactRoutes);
router.use('/categories', categoriesRoutes);

module.exports = router;
