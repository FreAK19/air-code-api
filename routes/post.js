const router = require('express-promise-router')();
const { body, param } = require('express-validator/check');

const passport = require('@/passport');
const postsController = require('@/controllers/post');
const validationMiddleware = require('@/validation/validationMiddleware');

router.route('/')
  .get(
    validationMiddleware,
    postsController.getAllPosts
  )
  .post(
    passport.authenticateAdmin,
    [
      body('title').exists(),
      body('description').exists(),
      body('type').exists(),
      body('technologies').exists()
    ],
    validationMiddleware,
    postsController.createPost
  );

router.route('/:post_id')
  .delete(
    passport.authenticateAdmin,
    [param('post_id').isMongoId()],
    validationMiddleware,
    postsController.deletePost
  );

router.route('/:post_id')
  .put(
    passport.authenticateAdmin,
    [param('post_id').isMongoId()],
    validationMiddleware,
    postsController.editPost
  );

module.exports = router;
