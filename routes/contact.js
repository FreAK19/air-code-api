const router = require('express-promise-router')();
const { body } = require('express-validator/check');

const passport = require('@/passport');
const contactController = require('@/controllers/contact');
const validationMiddleware = require('@/validation/validationMiddleware');

router.route('/')
	.get(
		validationMiddleware,
		contactController.getContact
	)
	.post(
		passport.authenticateAdmin,
		[
			body('address').exists(),
			body('mobile').exists(),
			body('email').exists(),
		],
		validationMiddleware,
		contactController.createContact
	);

module.exports = router;
