const Contact = require('@/models/Contact');

const defaultContactDto = {
	mobile: '',
	address: '',
	email: '',
	links: []
};

const getContact = (_req, res) => {
  return Contact.getContacts()
    .then(data => res.send(data ? data[data.length - 1] : defaultContactDto));
};

const createContact = ({ body }, res) => {
  const { address, mobile, email, links } = body;
  return Contact.create({ address, mobile, email, links }).then(data => res.send(data));
};

module.exports = {
	getContact,
	createContact,
};
