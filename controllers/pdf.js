const boom = require('boom');

const PDF = require('@/models/PDF');

const getLink = (_req, res) => {
  return PDF.getLink()
    .then(data => res.send(data.length > 0 ? data[0] : { link: ""}));
};

const updateLink = ({ params, body }, res) => {
  const newLink = { link, showCvLink } = body;

  return PDF.findOneAndUpdate(
    { _id: params.link_id },
    { $set: newLink },
    { new: true })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.send(boom.notFound('Link id not found'));
      }
    });
};

const createLink = ({ body }, res) => {
  const { link, showCvLink } = body;
  return PDF.create({ link }).then(data => res.send(data));
};


module.exports = {
	getLink,
	updateLink,
	createLink
};
