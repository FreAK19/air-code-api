const boom = require('boom');
const nodemailer = require('nodemailer');

const MailMessage = require('@/models/Mails');
const emailTemplate = require('@/helpers/mailMessage');

const testAccount = nodemailer.createTestAccount();

let transporter = nodemailer.createTransport({
  host: "smtp.ethereal.email",
  port: 587,
  secure: false,
  auth: {
    user: testAccount.user,
    pass: testAccount.pass
  }
});

const createAndSendMail = async ({ body }, res) => {
  const { email, message } = body;

  return await MailMessage.create({
    from: email,
    to: process.env.AUTHOR_EMAIL,
    message
  })
    .then(mailMessage => {
      
      if (mailMessage) {
        const mailOptions = {
          from: email,
          to: process.env.AUTHOR_EMAIL,
          subject: 'Submit message from portfolio',
          html: emailTemplate(email, message)
        };

        transporter.sendMail(mailOptions, (error) => {
          if (error) {
            res.send(error);
          } else {
            res.json({ text: 'Your message was send! I will write you in a 10 minutes' });
          }
        });
      }
    });
};

const getAllMailMessages = (_req, res) => {
  return MailMessage.getAllMails()
    .then(mails => {
      res.send({
        totalElements: mails.length,
        content: mails
      })
    });
};

const deleteMailMessage = ({ params }, res) => {
  return MailMessage.findOneAndRemove({ _id: params.mail_id })
    .then(mailMessage => {
      if (mailMessage) {
        res.send({ message: 'Message deleted' });
      } else {
        res.send(boom.notFound('Message id not found'));
      }
    });
};

module.exports = {
  createAndSendMail,
  deleteMailMessage,
  getAllMailMessages
};
