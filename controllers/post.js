const boom = require('boom');

const Post = require('@/models/Post');

const getAllPosts = (_req, res) => {
  return Post.getAllPosts()
    .then(posts => { 
      return res.send({
        totalElements: posts.length,
        content: posts,
      })
    });
};

const createPost = ({ body }, res) => {
  const { title, description, type, technologies, link, image, visible, shared, workinDate } = body;
  return Post.create({
    image,
    title,
    description,
    type,
    technologies,
    link,
    visible,
    shared,
    workinDate
  }).then(post => res.send(post));
};

const editPost = (req, res) => {
  const newPost = { ...req.body };

  return Post.findOneAndUpdate(
    { _id: req.params.post_id },
    { $set: newPost },
    { new: true })
    .then(post => {
      if (post) {
        res.send(post);
      } else {
        res.send(boom.notFound('Post id not found'));
      }
    });
};

const deletePost = ({ params }, res) => {
  return Post.findOneAndRemove({ _id: params.post_id })
    .then(post => {
      if (post) {
        res.send({ message: 'Post deleted' });
      } else {
        res.send(boom.notFound('Post id not found'));
      }
    });
};

module.exports = {
  getAllPosts,
  createPost,
  editPost,
  deletePost
};
