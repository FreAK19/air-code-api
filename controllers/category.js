const boom = require('boom');

const Categories = require('@/models/Categories');

const getCategories = (req, res) => {
  return Categories.getAllCategories()
    .then(categories => {
      return res.send({
        totalElements: categories.length,
        content: categories
      })
    });
};

const createCategory = ({ body }, res) => {
  const { name } = body;
	return Categories.create({ name })
		.then(category => res.send(category))
    .catch(err => {
      throw boom.badRequest('Dublicated name')
    });
};

const updateCategory = ({ params, body }, res) => {
  const newCategory = { name: body.name };

  return Categories.findOneAndUpdate(
    { _id: params.category_id },
    { $set: newCategory },
    { new: true })
    .then(category => {
      if (category) {
        return res.send(category);
      } else {
        return res.send(boom.notFound('Post id not found'));
      }
    });
};

const deleteCategory = ({ params }, res) => {
  return Categories.findOneAndRemove({ _id: params.category_id })
    .then(category => {
      if (category) {
        return res.send({ message: 'Category deleted' });
      } else {
        return res.send(boom.notFound('Category id not found'));
      }
    });
};

module.exports = {
  getCategories,
  createCategory,
  updateCategory,
  deleteCategory
};
