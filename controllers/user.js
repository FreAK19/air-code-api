const boom = require('boom');
const jwt = require('jsonwebtoken');

const User = require('@/models/User');

const login = ({ body }, res) => {
  return User.getUserByEmailAndPassword(body.email, body.password)
    .then(user => {
      const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
        expiresIn: '7d'
      });
      res.send({ token });
    })
    .catch(() => {
      throw boom.badData('Email/password pair is not valid')
    });
};

module.exports = {
  login
};
