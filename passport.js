const boom = require('boom');
const passport = require('passport');
const passportJwt = require('passport-jwt');

const User = require('@/models/User');

const ExtractJwt = passportJwt.ExtractJwt;
const JwtStrategy = passportJwt.Strategy;

const passportOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET
};

const strategy = new JwtStrategy(passportOptions, (jwt_payload, next) => {
  User.getById(jwt_payload.id)
    .then((user) => {
      if (user) {
        next(null, user);
      } else {
        next(null, false);
      }
    })
    .catch(() => {
      next(null, false);
    });
});

passport.use(strategy);

const authenticateAdmin = async (req, res, next) => {    
  try {
    await passport.authenticate('jwt', { session: false }, function(err, user) {
      if (err) {
        return next(err);
      }
  
      if (user) {      
        req.user = user;
      } else {
        throw res.send(boom.unauthorized('Unauthorized'));
      }
  
      return next();
  
    })(req, res, next);
  } catch (error) {
    return next(err);
  }
};

module.exports = {
  authenticateAdmin
};
