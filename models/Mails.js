const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const mailSchema = new Schema({
  to: { type: String },
  message: { type: String, require: true },
  from: { type: String, require: true, unique: false },
  created_at: { type: Date, required: true, default: Date.now }
});

mailSchema.statics = {
  getAllMails () {
    return this.find().exec();
  }
};

module.exports = mongoose.model('MailMessage', mailSchema);
