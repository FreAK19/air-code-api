const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const categorySchema = new Schema({
	name: { required: true, type: String, unique: 'Such category exist'},
	created_at: { type: Date, required: true, default: Date.now }
});

categorySchema.statics = {
  getAllCategories() {
    return this.find()
      .sort({ created_at: 'desc' })
      .exec()
      .then(data => data);
  }
};

module.exports = mongoose.model('Category', categorySchema);
