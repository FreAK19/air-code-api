const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  password: { type: String },
  created_at: { type: Date, required: true, default: Date.now },
  email: { type: String, require: true, unique: 'This email is already used' }
});

userSchema.plugin(uniqueValidator);

userSchema.statics = {
  getById (id) {
    return this.findOne({ _id: id })
      .select({ password: 0 })
      .exec()
      .then(user => user);
  },
  getUserByEmailAndPassword (email, password) {
    return this.findOne({ email: email.toLowerCase() })
      .exec()
      .then(user => {
        return new Promise((resolve, reject) => {
          if (user.password === password) {
            resolve(user);
          } else {
            reject({});
          }
        });
      });
  }
};

module.exports = mongoose.model('User', userSchema);
