const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const pdfSchema = new Schema({
	link: { required: true, type: String},
	showCvLink: { required: false, type: Boolean},
});

pdfSchema.statics = {
  getLink() {
    return this.find()
      .exec()
      .then(data => data);
  }
};

module.exports = mongoose.model('PDF', pdfSchema);
