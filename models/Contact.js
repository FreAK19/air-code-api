const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const contactSchema = new Schema({
	address: { required: true, type: String},
	mobile: { required: true, type: String},
	email: { required: true, type: String },
	links: [{
		link: { type: String },
		icon: { type: String },
		title: { type: String }
	}]
});


contactSchema.statics = {
  getContacts() {
    return this.find()
      .exec()
      .then(data => data);
  }
};

module.exports = mongoose.model('Contact', contactSchema);
