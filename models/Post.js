const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const postSchema = new Schema({
  technologies: [String],
  image: {
    alternate_text: String,
    src: { type: String, required: true }
  },
  description: { type: String, required: true },
  link: { type: String, required: false },
  title: { type: String, required: true },
  shared: { type: Boolean, required: false },
  visible: { type: Boolean, required: false },
  created_at: { type: Date, required: true, default: Date.now() },
  type: { type: String, required: false, default: 'DEVELOPMENT' },
  created_at: { type: Date, required: true, default: Date.now },
  workinDate: { type: Date, required: false, default: new Date().toISOString().substr(0, 10) },
});


postSchema.statics = {
  getAllPosts() {
    return this.find()
      .sort({ created_at: 'desc' })
      .exec()
      .then(posts => posts);
  }
};

module.exports = mongoose.model('Post', postSchema);
