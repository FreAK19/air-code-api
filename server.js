require('dotenv').config();
require('./moduleAlias');
const cors = require('cors');
const boom = require('boom');
const morgan = require('morgan');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const compression = require('compression');

const router = require('@/routes');

const app = express();
const http = require('http').Server(app);

mongoose.Promise = Promise;
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
mongoose.connect(process.env.MONGODB_URL, { useNewUrlParser: true, useUnifiedTopology: true });

app.use(cors());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", '*');
  res.header("Access-Control-Allow-Credentials", true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
  next();
});
app.use(compression());
app.use(morgan('common'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', router);

app.use(() => {
  throw boom.notFound('Not found');
});

app.use((err, req, res) => {
  err.output.payload.data = err.data;
  return res.status(err.output.statusCode).json(err.output.payload);
});

http.listen(process.env.PORT);
