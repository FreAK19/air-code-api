module.exports = {
  production: {
    SERVER_DOMAIN: "https://mokup.herokuapp.com"
  },
  development: {
    SERVER_DOMAIN: "http://localhost:3001"
  }
};
