const boom = require('boom');
const { validationResult } = require('express-validator/check');

function validationMiddleware(req, res, next) {
  const errors = validationResult(req).formatWith(({ msg }) => msg);
  if (!errors.isEmpty()) {
    return res.send(boom.badData('Data is not valid', errors.mapped()));
  }
  return next();
}

module.exports = validationMiddleware;
